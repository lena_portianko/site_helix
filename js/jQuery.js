$(function() {
    var tab = $('#tabs .Rectangle_Bringing_tabs_items > div'); 
    tab.hide().filter(':first').show(); 
    
    $('#tabs .Rectangle_Bringing_tabs_nav a').click(function(){
        tab.hide(); 
        tab.filter(this.hash).show(); 
        $('#tabs .Rectangle_Bringing_tabs_nav a').removeClass('active');
        $(this).addClass('active');
        return false;
    }).filter(':first').click();

    $('.Rectangle_Bringing_tabs_nav a').on('click', function() {
        $('.Rectangle_Bringing_tabs_nav a').removeClass('active');
        $(this).addClass('active');
    });

    $('.Rectangle_Future_content_text_accordion_block').hide();
    $('.Rectangle_Future_content_text_accordion_block:first').show();

    $('.Rectangle_Future_content_text_accordion_header').on('click', function(){
        $(this).toggleClass('in').next().slideToggle();
        $('.Rectangle_Future_content_text_accordion_header').not(this).removeClass('in').next().slideUp();

        $('.Rectangle_Future_content_img').attr('src', 'img/' + $(this).attr('id') + '.png');
        
    });
});


